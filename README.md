
Quais softwares são necessários ?

| programa | versão
|   --     |   --
| docker   | 18.09.1+
| docker-compose | 1.21+

Quais benefícios ele oferece ?

- Banco de dados MariaDB versão latest
- Adminer versão latest

Configurações Banco de dados

    Porta   : 30306
    Usuário : root
    Senha   : desenvolvimento

Como subir banco de dados ?

    docker-compose -f docker-compose.yml up

Como acessar serviço adminer ?

- url : [localhost:/30305](http://localhost:30305/)

Como é a tela de login ?

![Entrar Adminer](imagens/entrar-adminer.png "Entrar Adminer")

Como é a tela inicial ?

![Entrar Adminer](imagens/tela-inicial-adminer.png "Entrar Adminer")


Como interromper banco de dados ?

    ctrl +c

Como parar banco de dados ?

    docker-compose -f docker-compose.yml down

Como deletar volume de dados ?

    docker volume rm banco-de-dados-local-volume